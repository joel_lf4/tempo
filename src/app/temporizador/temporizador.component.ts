import { IfStmt } from '@angular/compiler';
import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-temporizador',
  templateUrl: './temporizador.component.html',
  styleUrls: ['./temporizador.component.css']
})
export class TemporizadorComponent implements OnInit {

  public horas: number = 0;
  public minutos: number = 45;
  public segundos: number = 0;
  private timer: any;
  private date = new Date();

  public show: boolean = true;
  public disabled: boolean = false;
  // public animate: boolean = false
  public finished: boolean = false;
  public littleTime: boolean = false;

   


  constructor() { }

  ngOnInit(): void {
  }

  increment(type: 'M' | 'H' | 'S' ) {
    if (type === 'H') {
      if (this.horas >= 99) return;
      this.horas +=1;
    }
    else if (type === 'M') {
      if (this.minutos >=59) return;
      this.minutos +=1;
    }else{
      if(this.segundos >= 59)return;
      this.segundos +=1;
    }
  }

  decrement(type: 'M' | 'H' | 'S' ) {
    if (type === 'H') {
      if (this.horas <= 0) return;
      this.horas -=1;
    }
    else if (type === 'M') {
      if (this.minutos <=0) return;
      this.minutos -=1;
    }else{
      if(this.segundos <= 0)return;
      this.segundos -=1;
    }
  }

  updateTimer(){
    this.date.setHours(this.horas);
    this.date.setMinutes(this.minutos);
    this.date.setSeconds(this.segundos);
    this.date.setMilliseconds(0);
    const time = this.date.getTime();

    if(this.finished){
      this.date.setTime (time + 1000);
    }else{
      this.date.setTime (time - 1000);
    }

    
    this.horas = this.date.getHours();
    this.minutos = this.date.getMinutes();
    this.segundos = this.date.getSeconds();
    
    if(this.minutos < 10){
      this.littleTime = true;
    }

    if (this.horas == 0 && this.minutos == 0 && this.segundos == 0) {
      this.finished = true;
    }
  }

  stop(){
    this.disabled = false;
    this.show = true;
    // this.animate = false;
    clearInterval(this.timer);
  }

  reset(){
    this.horas = 0;
    this.minutos = 0;
    this.segundos = 0;
    this.stop();

  }

  start(){
    if(this.horas > 0 || this.minutos > 0 || this.segundos > 0){
      this.disabled = true;
      this.show = false;
      this.updateTimer();

      if(this.segundos > 0){
        this.timer = setInterval(() => {
          this.updateTimer();
        }, 1000);
      }
    }
  }

}
